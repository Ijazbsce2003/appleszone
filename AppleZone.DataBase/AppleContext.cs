﻿using AppleZone.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppleZone.DataBase
{
    public class AppleContext : DbContext
    {
        public AppleContext() : base("AppleConnection")
        {

        }

        public DbSet<Products> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
    }
}
