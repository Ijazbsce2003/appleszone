﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AppleZone.web.Startup))]
namespace AppleZone.web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
